<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\Offices\Models\OfficeModel;
use Proexe\BookingApp\Utilities\DistanceCalculator;

class CalculateDistanceToOffice extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateDistanceToOffice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates distance to office';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $distanceCalculator = new DistanceCalculator();
        $offices = OfficeModel::all()->toArray();

        $lat = $this->ask('Enter Lat:', '14.12232322');
        $lng = $this->ask('Enter Lng:', '8.12232322');
        $unit = $this->choice('Unit',
            [DistanceCalculator::KILOMETERS, DistanceCalculator::METERS],
            DistanceCalculator::KILOMETERS);

        $distanceCalculator
            ->setFrom(['lat' => $lat, 'lng' => $lng])
            ->setOffices($offices);

        foreach ($offices as $index => $office) {
            $distance = $distanceCalculator->calculate($index, $unit);

            $office['distance'] = $distance;

            $this->line('Distance to '.$office['name'].': '.$distance);
        }

        $closestOffice = $distanceCalculator->findClosestOffice();

        $this->line('Closest office: '.$closestOffice['name']);
    }
}
