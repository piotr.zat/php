<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Carbon\Carbon;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{
    /**
     * I can change ResponseTimeCalculatorInterface for typehint
     *
     * @param  string  $bookingDateTime
     * @param  string  $responseDateTime
     * @param  array  $officeHours
     *
     * @return int
     *
     * @throws \Exception
     */
    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        $minutes = 0;

        $datesInRange = $this->getDatesFromRange($bookingDateTime, $responseDateTime);

        foreach ($datesInRange as $dateInRange) {

            if ($this->shouldAddMinuteToResponse($dateInRange, $officeHours)) {
                $minutes++;
            }
        }

        return $minutes;
    }

    /**
     * @param  \DateTimeInterface  $dateInRange
     * @param  array  $officeHours
     *
     * @return bool
     *
     * @throws \Exception
     */
    private function shouldAddMinuteToResponse(\DateTimeInterface $dateInRange, array $officeHours): bool
    {
        $dayOfTheWeek = $dateInRange->format('w');
        $officeHoursInDay = $officeHours[$dayOfTheWeek];

        if ($officeHoursInDay['isClosed']) {
            return false;
        }

        $openingHour = new \DateTime($dateInRange->format('Y-m-d ').$officeHoursInDay['from']);
        $closingHour = new \DateTime($dateInRange->format('Y-m-d ').$officeHoursInDay['to']);

        return ($openingHour <= $dateInRange && $dateInRange < $closingHour);
    }

    /**
     * @param  string  $start
     * @param  string  $end
     *
     * @return array
     *
     * @throws \Exception
     */
    private function getDatesFromRange(string $start, string $end): array
    {
        $datesInRanges = [];

        // Variable that store the date interval
        // of period 1 minute
        $interval = new \DateInterval('PT1M');

        $end = new \DateTime($end);

        $period = new \DatePeriod(new \DateTime($start), $interval, $end);

        foreach ($period as $date) {
            $datesInRanges[] = $date;
        }

        // Return the array elements
        return $datesInRanges;
    }

}