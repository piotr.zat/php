<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

/**
 * Class DistanceCalculator
 *
 * For finding closest office in pure SQL I can use :
 *
 * SELECT offices.*,
 *        (6371 * acos(
 *                cos( radians(offices.lat) )
 *                * cos( radians( 14.12232322 ) )
 *                * cos( radians( 8.12232322 ) - radians(offices.lng) )
 *                + sin( radians(offices.lat) )
 *                * sin( radians( 14.12232322 ) )
 *            ) ) AS distance_to_given_point
 * FROM offices
 * ORDER BY distance_to_given_point ASC
 * LIMIT 1
 *
 * tested in MySQL.
 *
 * For bigger databases I would prefer to use OpenGIS in MySQL or PostGIS for PostgreSQL.
 *
 * I decided to move $offices and $from to class fields, and now I can calculate distance only once
 * what can have significant impact on optimization in larger database.
 *
 * @package Proexe\BookingApp\Utilities
 */
class DistanceCalculator
{
    const METERS = 'm';
    const KILOMETERS = 'km';

    /**
     * @var array
     */
    private $offices;

    /**
     * @var array
     */
    private $from;

    /**
     * @param  int  $index
     * @param  string  $unit  - m, km
     *
     * @return float
     */
    public function calculate(int $index, string $unit = self::METERS): float
    {
        $earthRadius = $this->getEarthRadius($unit);

        $distance = $this->vincentyGreatCircleDistance(
            $this->from['lat'],
            $this->from['lng'],
            $this->offices[$index]['lat'],
            $this->offices[$index]['lng'],
            $earthRadius
        );

        $this->offices[$index]['distance'] = $distance;

        return $distance;
    }

    /**
     * @return array
     */
    public function findClosestOffice(): array
    {
        $closestIndex = null;
        $closestDistance = null;

        foreach ($this->offices as $index => $office) {
            // this is added to check if array was in calculate
            if (array_key_exists('distance', $office)) {
                $distance = $office['distance'];
            } else {
                $distance = $this->calculate($index);
            }

            if (!$closestDistance || $distance < $closestDistance) {
                $closestDistance = $distance;
                $closestIndex = $index;
            }
        }
        return $this->offices[$closestIndex];
    }

    /**
     * @param  array  $offices
     * @return $this
     */
    public function setOffices(array $offices): self
    {
        $this->offices = $offices;

        return $this;
    }

    /**
     * @param  array  $from
     * @return $this
     */
    public function setFrom(array $from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get earth radius depending on choosen unit
     * @param  string  $unit
     *
     * @return int
     */
    private function getEarthRadius(string $unit = self::METERS): int
    {
        switch ($unit) {
            case self::METERS:
                return 6371000;
                break;
            case self::KILOMETERS:
                return 6371;
                break;
            default:
                throw new \InvalidArgumentException('You should define either meters or kilometers');
        }
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param  float  $latitudeFrom  Latitude of start point in [deg decimal]
     * @param  float  $longitudeFrom  Longitude of start point in [deg decimal]
     * @param  float  $latitudeTo  Latitude of target point in [deg decimal]
     * @param  float  $longitudeTo  Longitude of target point in [deg decimal]
     * @param  int  $earthRadius  Mean earth radius in [m]
     *
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function vincentyGreatCircleDistance(
        float $latitudeFrom,
        float $longitudeFrom,
        float $latitudeTo,
        float $longitudeTo,
        int $earthRadius = 6371
    ): float {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return (float) ($angle * $earthRadius);
    }

}