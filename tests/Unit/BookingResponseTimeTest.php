<?php

namespace Tests\Unit;

use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testTask4($bookingDateTime, $responseDateTime, $openingHours, $expectedMinutes)
    {
        $calculator = new ResponseTimeCalculator();

        $this->assertEquals($expectedMinutes,
            $calculator->calculate($bookingDateTime, $responseDateTime, $openingHours));
    }

    public function additionProvider()
    {
        return [
            // start in point of opening hours, ending in office hours
            [
                '2020-05-17 08:00',
                '2020-05-17 08:05',
                [
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ]
                ],
                5,
            ],

            // starting in  opening hours, ending in point of ending hours
            [
                '2020-05-17 15:00',
                '2020-05-17 16:00',
                [
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ]
                ],
                60,
            ],

            // start and end in office hours (additional in different day, and closed day between)
            // example form Task 4 .md DESCRIPTION
            [
                '2018-08-04 16:00:00',
                '2018-08-06 09:30:00',
                [
                    [
                        "isClosed" => true,
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ]
                ],
                150,
            ],

            // start in ending hours and ending in office hours
            [
                '2018-08-04 18:00:00',
                '2018-08-06 09:30:00',
                [
                    [
                        "isClosed" => true,
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "09:00",
                        "to" => "18:00",
                    ]
                ],
                30,
            ],
            // start in point of opening hours, ending in point of ending hours
            [
                '2020-05-17 07:00',
                '2020-05-17 16:00',
                [
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ]
                ],
                480,
            ],
            // start in point of opening hours, ending in point of ending hours in two days
            [
                '2020-05-17 07:00',
                '2020-05-18 16:00',
                [
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => false,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ],
                    [
                        "isClosed" => true,
                        "from" => "08:00",
                        "to" => "16:00",
                    ]
                ],
                960,
            ]
        ];
    }

}
